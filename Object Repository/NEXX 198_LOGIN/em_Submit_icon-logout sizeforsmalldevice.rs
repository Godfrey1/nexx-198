<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>em_Submit_icon-logout sizeforsmalldevice</name>
   <tag></tag>
   <elementGuidId>f7022582-1671-4502-9965-20abdf971b89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='main-body']/app-root/app-layout/div/app-header/nav/div/div[2]/ul/li[3]/a/em</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>em</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>icon-logout sizeforsmalldevice</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body&quot;)/app-root[@class=&quot;layout-fixed&quot;]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;wrapper&quot;]/app-header[@class=&quot;topnavbar-wrapper&quot;]/nav[@class=&quot;navbar topnavbar&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-7 col-xs-6 col-sm-6 col-md-4 col-lg-5 search-list1&quot;]/ul[@class=&quot;navbar-nav flex-row mr-autoCsmCSS&quot;]/li[@class=&quot;nav-item  d-md-block&quot;]/a[@class=&quot;nav-link&quot;]/em[@class=&quot;icon-logout sizeforsmalldevice&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='main-body']/app-root/app-layout/div/app-header/nav/div/div[2]/ul/li[3]/a/em</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submit'])[1]/preceding::em[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[1]/preceding::em[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a/em</value>
   </webElementXpaths>
</WebElementEntity>
