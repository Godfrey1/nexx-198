<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Axin insurance</name>
   <tag></tag>
   <elementGuidId>42caddd1-6cc2-4168-a85e-771c7c75f363</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='main-body']/app-root/app-layout/div/section/div/app-sales/div/div/div[4]/div/div/div/ul/li[8]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Axin insurance</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body&quot;)/app-root[@class=&quot;layout-fixed&quot;]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;wrapper&quot;]/section[@class=&quot;section-container&quot;]/div[@class=&quot;content-wrapper&quot;]/app-sales[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;card bottomspace&quot;]/div[@class=&quot;card-body tenant&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;sales-card-product-div-parent&quot;]/div[@class=&quot;sales-card-product-div&quot;]/ul[@class=&quot;list-unstyled&quot;]/li[@class=&quot;ng-star-inserted&quot;]/p[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='main-body']/app-root/app-layout/div/section/div/app-sales/div/div/div[4]/div/div/div/ul/li[8]/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='YZ'])[1]/following::p[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VWX'])[1]/following::p[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product'])[1]/preceding::p[34]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty'])[1]/preceding::p[34]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/ul/li[8]/p</value>
   </webElementXpaths>
</WebElementEntity>
