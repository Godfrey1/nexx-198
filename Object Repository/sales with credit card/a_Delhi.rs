<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Delhi</name>
   <tag></tag>
   <elementGuidId>63aaf17e-d695-4c41-96a4-763039a9e7ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='main-body']/app-root/app-layout/div/app-header/nav/div/div[3]/ul/li/div/div[24]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-item</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Delhi</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body&quot;)/app-root[@class=&quot;layout-fixed&quot;]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;wrapper&quot;]/app-header[@class=&quot;topnavbar-wrapper&quot;]/nav[@class=&quot;navbar topnavbar&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-5 col-xs-6 col-sm-6 col-md-5 col-lg-5&quot;]/ul[@class=&quot;navbar-nav flex-row columnsmall1&quot;]/li[@class=&quot;nav-item dropdown dropdown-list branchName ng-star-inserted open show&quot;]/div[@class=&quot;dropdown-menu animated fadeInDown newcss show ng-star-inserted&quot;]/div[@class=&quot;ng-star-inserted&quot;]/a[@class=&quot;dropdown-item&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='main-body']/app-root/app-layout/div/app-header/nav/div/div[3]/ul/li/div/div[24]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Delhi')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[24]/a</value>
   </webElementXpaths>
</WebElementEntity>
