import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.callTestCase(findTestCase('begin'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('sale with cheque/p_Axin insurance'))

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('sale with cheque/input_Pack Quantity_quantity'), '1')

not_run: WebUI.setText(findTestObject('sale with cheque/input_Cost Price_costPrice'), '15')

not_run: WebUI.setText(findTestObject('sale with cheque/input_Sell Price_sellPrice'), '20')

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('sale with cheque/button_OK'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('sale with cheque/input'), 'Kenya')

not_run: WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('sales with credit card/credit2/input'), 'Armenian')

not_run: WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

not_run: WebUI.setText(findTestObject('sale with cheque/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '1233')

not_run: WebUI.setText(findTestObject('sale with cheque/input_Customer Address_form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '144')

not_run: WebUI.click(findTestObject('sale with cheque/button_Proceed To Payment'), FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('sale with cheque/button_Cheque'))

WebUI.setText(findTestObject('sale with cheque/input_Amount_payAmountInput'), '20')

WebUI.setText(findTestObject('sale with cheque/input_Cheque Number_i'), '22222')

WebUI.delay(4)

WebUI.setText(findTestObject('sale with cheque/input_Cheque Date_dp-basic'), '22')

not_run: WebUI.click(findTestObject('sale with cheque/span_22'))

WebUI.click(findTestObject('sale with cheque/span_Enter'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sale with cheque/button_Pay'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.delay(2)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

