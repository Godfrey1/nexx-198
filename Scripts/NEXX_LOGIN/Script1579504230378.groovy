import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('http://192.168.13.198:9632/#/')

WebUI.setText(findTestObject('NEXX 198_LOGIN/input_This field is required_userName'), 'sateesh')

WebUI.delay(4)

WebUI.click(findTestObject('NEXX 198_LOGIN/button_Next'))

WebUI.setEncryptedText(findTestObject('NEXX 198_LOGIN/input_Login_password'), 'aeHFOx8jV/A=')

WebUI.delay(4)

WebUI.click(findTestObject('NEXX 198_LOGIN/button_Login'))

WebUI.delay(5)

not_run: WebUI.click(findTestObject('NEXX 198_LOGIN/em_Submit_icon-logout sizeforsmalldevice'))

