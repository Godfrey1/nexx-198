import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('sale with cash/em_Submit_icon-grid sizeforsmalldevice'))

not_run: WebUI.click(findTestObject('sale with cash/a_Web POS'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

not_run: WebUI.click(findTestObject('sale with cash/span_Sale'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

not_run: WebUI.click(findTestObject('sale with cash/p_Axin insurance'))

not_run: WebUI.delay(6)

not_run: WebUI.setText(findTestObject('sale with cash/input_Pack Quantity_quantity'), '1')

not_run: WebUI.setText(findTestObject('sale with cash/input_Cost Price_costPrice'), '15')

not_run: WebUI.setText(findTestObject('sale with cash/input_Sell Price_sellPrice'), '20')

not_run: WebUI.delay(6)

not_run: WebUI.click(findTestObject('Page_Login/button_OK'))

not_run: WebUI.setText(findTestObject('sale with cash/input'), 'Argentina')

not_run: WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

not_run: WebUI.delay(5)

not_run: WebUI.setText(findTestObject('sale with cash/sale2/input'), 'Armenian')

not_run: WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

not_run: WebUI.setText(findTestObject('sale with cash/input_Customer Address_form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '22')

not_run: WebUI.click(findTestObject('sale with cash/button_Proceed To Payment'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sale with cash/button_Cash'))

WebUI.setText(findTestObject('sale with cash/input_Amount_payAmountInput'), '400')

WebUI.click(findTestObject('sale with cash/span_Enter'))

WebUI.click(findTestObject('sale with cash/button_Pay'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.delay(2)

WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(1)

