import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('sales with credit card/em_Merchandising_icon-grid sizeforsmalldevice'))

not_run: WebUI.click(findTestObject('sales with credit card/a_Web POS'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

not_run: WebUI.click(findTestObject('sales with credit card/span_AHMEDABAD CANADA'))

not_run: WebUI.click(findTestObject('sales with credit card/a_Delhi'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('sales with credit card/credit2/span_Sale'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

not_run: WebUI.delay(3)

WebUI.click(findTestObject('sales with credit card/p_Aig'))

WebUI.delay(2)

WebUI.setText(findTestObject('sales with credit card/input_Sell Price_sellPrice'), '400')

WebUI.delay(4)

WebUI.click(findTestObject('sales with credit card/credit2/button_OK'))

WebUI.delay(2)

WebUI.setText(findTestObject('sales with credit card/input__attr00'), '122')

WebUI.setText(findTestObject('sales with credit card/input__attr10'), '132')

WebUI.setText(findTestObject('sales with credit card/input__attr20'), '365')

WebUI.setText(findTestObject('sales with credit card/input__attr30'), '1258')

WebUI.delay(2)

WebUI.click(findTestObject('NEXX 198_LOGIN/button_OK'))

WebUI.setText(findTestObject('sales with credit card/input'), 'Argentina')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.setText(findTestObject('sales with credit card/credit2/input'), 'Kenyan')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.setText(findTestObject('sales with credit card/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '3257')

WebUI.setText(findTestObject('sales with credit card/input_Customer Address_form-control tenant ng-pristine ng-valid ng-star-inserted ng-touched'), 
    '1254')

WebUI.click(findTestObject('sales with credit card/button_Proceed To Payment'))

