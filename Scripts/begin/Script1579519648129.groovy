import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sales with credit card/em_Merchandising_icon-grid sizeforsmalldevice'))

WebUI.click(findTestObject('sales with credit card/a_Web POS'))

WebUI.click(findTestObject('sale with cash/button_OK'))

WebUI.click(findTestObject('sales with credit card/span_AHMEDABAD CANADA'))

WebUI.click(findTestObject('sales with credit card/a_Delhi'))

WebUI.delay(2)

WebUI.click(findTestObject('sales with credit card/credit2/span_Sale'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

