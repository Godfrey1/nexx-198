import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sales with credit card/em_Merchandising_icon-grid sizeforsmalldevice'))

WebUI.click(findTestObject('sales with credit card/a_Web POS'))

WebUI.click(findTestObject('sale with cash/button_OK'))

WebUI.click(findTestObject('sales with credit card/span_AHMEDABAD CANADA'))

WebUI.click(findTestObject('sales with credit card/a_Delhi'))

WebUI.delay(2)

WebUI.click(findTestObject('sales with credit card/credit2/span_Sale'))

not_run: WebUI.click(findTestObject('sale with cash/button_OK'))

WebUI.click(findTestObject('sales with credit card/p_Aig'))

WebUI.delay(4)

WebUI.setText(findTestObject('sales with credit card/input_Sell Price_sellPrice'), '400')

WebUI.delay(4)

WebUI.click(findTestObject('sales with credit card/credit2/button_OK'))

WebUI.delay(2)

WebUI.setText(findTestObject('sales with credit card/input__attr00'), '122')

WebUI.setText(findTestObject('sales with credit card/input__attr10'), '132')

WebUI.setText(findTestObject('sales with credit card/input__attr20'), '147')

WebUI.setText(findTestObject('sales with credit card/input__attr30'), '1258')

WebUI.delay(2)

WebUI.click(findTestObject('NEXX 198_LOGIN/button_OK'))

WebUI.delay(5)

not_run: WebUI.click(findTestObject('cash and cheque/p_Axin insurance'))

not_run: WebUI.setText(findTestObject('cash and cheque/input_Pack Quantity_quantity'), '1')

not_run: WebUI.setText(findTestObject('cash and cheque/input_Sell Price_sellPrice'), '20')

not_run: WebUI.setText(findTestObject('cash and cheque/input_Cost Price_costPrice'), '10')

not_run: WebUI.click(findTestObject('cash and cheque/button_OK'))

WebUI.setText(findTestObject('sales with credit card/input'), 'Argentina')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sales with credit card/credit2/input'), 'Kenyan')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sales with credit card/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '3257')

WebUI.setText(findTestObject('sales with credit card/input_Customer Address_form-control tenant ng-pristine ng-valid ng-star-inserted ng-touched'), 
    '1254')

WebUI.clickOffset(findTestObject('sales with credit card/button_Proceed To Payment'), 0, 0)

not_run: WebUI.click(findTestObject('cash credit/button_Cash'))

WebUI.delay(3)

WebUI.setText(findTestObject('cash and cheque/input_Amount_payAmountInput'), '20')

WebUI.click(findTestObject('cash and cheque/span_Enter'))

WebUI.click(findTestObject('cash and cheque/button_Cheque'))

WebUI.delay(4)

WebUI.setText(findTestObject('cash and cheque/input_Amount_payAmountInput'), '380')

WebUI.setText(findTestObject('cash and cheque/input_Cheque Number_i'), '2020')

WebUI.setText(findTestObject('sale with cheque/input_Cheque Date_dp-basic'), '22')

WebUI.click(findTestObject('cash and cheque/span_Enter'))

WebUI.delay(5)

WebUI.click(findTestObject('cash credit/button_Pay'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.closeWindowIndex('1')

WebUI.delay(2)

WebUI.switchToWindowIndex(0)

not_run: WebUI.delay(1)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(1)

WebUI.click(findTestObject('sale with cash/span_Sale'))

WebUI.delay(2)

WebUI.callTestCase(findTestCase('SALE with cash'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('cash credit card'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('credit and cheque'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('cash credit and cheque'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4)

WebUI.callTestCase(findTestCase('invoice customer'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('discount and cash'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('saleswith creditcard'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('credit note'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Main Test Cases/cheque'), [:], FailureHandling.STOP_ON_FAILURE)

