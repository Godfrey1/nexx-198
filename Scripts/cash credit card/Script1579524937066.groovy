import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('cash credit/button_Cash'))

WebUI.setText(findTestObject('cash credit/input_Amount_payAmountInput'), '20')

WebUI.click(findTestObject('cash credit/span_Enter'))

WebUI.click(findTestObject('cash credit/button_Credit Card'))

WebUI.setText(findTestObject('cash credit/input_Amount_payAmountInput'), '380')

WebUI.setText(findTestObject('cash credit/input_Card Number_focusValue0'), '2020')

WebUI.setText(findTestObject('cash credit/input_Auth Code_i'), '022')

WebUI.setText(findTestObject('cash credit/input_Expiry Date_focusValue1'), 'Feb')

WebUI.click(findTestObject('cash credit/span_Enter'))

WebUI.click(findTestObject('cash credit/button_Pay'))

WebUI.closeWindowIndex(1)

WebUI.delay(2)

WebUI.switchToWindowIndex(0)

WebUI.delay(2)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(5)

WebUI.click(findTestObject('sale with cash/span_Sale'))

