import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('credit card and cheque/button_Credit Card'))

WebUI.setText(findTestObject('credit card and cheque/input_Amount_payAmountInput'), '200')

WebUI.setText(findTestObject('credit card and cheque/input_Card Number_focusValue0'), '202022')

WebUI.setText(findTestObject('credit card and cheque/input_Auth Code_i'), '2020')

WebUI.setText(findTestObject('credit card and cheque/input_Expiry Date_focusValue1'), '22')

WebUI.click(findTestObject('credit card and cheque/span_Enter'))

WebUI.click(findTestObject('credit card and cheque/button_Cheque'))

WebUI.setText(findTestObject('credit card and cheque/input_Amount_payAmountInput'), '200')

WebUI.setText(findTestObject('credit card and cheque/input_Cheque Number_i'), '2222010')

WebUI.setText(findTestObject('credit card and cheque/input_Cheque Date_dp-basic'), 'Mar')

WebUI.click(findTestObject('credit card and cheque/span_Enter'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('credit card and cheque/button_Pay'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(2)

WebUI.click(findTestObject('sale with cheque/span_Sale'))

