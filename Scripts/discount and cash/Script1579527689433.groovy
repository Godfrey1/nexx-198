import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('aig'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('discount and cash/button_Discount'))

WebUI.setText(findTestObject('discount and cash/input_Discount_payAmountInput'), '15')

WebUI.click(findTestObject('discount and cash/span_Enter'))

WebUI.click(findTestObject('discount and cash/button_Cash'))

WebUI.setText(findTestObject('discount and cash/input_Amount_payAmountInput'), '385')

WebUI.click(findTestObject('sale with cheque/span_Enter'))

WebUI.click(findTestObject('discount and cash/button_Pay'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(1)

WebUI.click(findTestObject('sale with cash/span_Sale'))

