import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('begin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('invoiced customer/input'), 'Corporate')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('cash credit/input'), 'Pt Aig Insurance Indonesia')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('credit note/input'), 'Vasco Worldwide')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('sales with credit card/p_Aig'))

WebUI.delay(2)

WebUI.setText(findTestObject('sales with credit card/input_Sell Price_sellPrice'), '400')

WebUI.delay(4)

WebUI.click(findTestObject('sales with credit card/credit2/button_OK'))

WebUI.delay(2)

WebUI.setText(findTestObject('sales with credit card/input__attr00'), '122')

WebUI.setText(findTestObject('sales with credit card/input__attr10'), '132')

WebUI.setText(findTestObject('sales with credit card/input__attr20'), '147')

WebUI.setText(findTestObject('sales with credit card/input__attr30'), '1258')

WebUI.delay(2)

WebUI.click(findTestObject('NEXX 198_LOGIN/button_OK'))

WebUI.setText(findTestObject('sales with credit card/input'), 'Argentina')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sales with credit card/credit2/input'), 'Kenyan')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sales with credit card/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '3257')

WebUI.setText(findTestObject('sales with credit card/input_Customer Address_form-control tenant ng-pristine ng-valid ng-star-inserted ng-touched'), 
    '1274')

WebUI.click(findTestObject('sales with credit card/button_Proceed To Payment'))

WebUI.delay(2)

WebUI.click(findTestObject('invoiced customer/button_Yes'))

not_run: WebUI.delay(2)

WebUI.setText(findTestObject('invoiced customer/input_Discount_payAmountInput'), '10')

WebUI.click(findTestObject('invoiced customer/span_Enter'))

WebUI.delay(5)

WebUI.click(findTestObject('invoiced customer/button_Pay'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.click(findTestObject('cash and cheque/button_Continue'))

WebUI.delay(2)

WebUI.click(findTestObject('NEXX 198_LOGIN/em_Submit_icon-logout sizeforsmalldevice'))

