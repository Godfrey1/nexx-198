import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('NEXX_LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('begin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sale with cheque/p_Axin insurance'))

WebUI.delay(2)

WebUI.setText(findTestObject('sale with cheque/input_Pack Quantity_quantity'), '1')

WebUI.setText(findTestObject('sale with cheque/input_Cost Price_costPrice'), '15')

WebUI.setText(findTestObject('sale with cheque/input_Sell Price_sellPrice'), '20')

WebUI.click(findTestObject('Page_Login/button_OK'))

WebUI.setText(findTestObject('sale with cheque/input'), 'Kenya')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sales with credit card/credit2/input'), 'Armenian')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('sale with cheque/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '1233')

WebUI.setText(findTestObject('sale with cheque/input_Customer Address_form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '144')

WebUI.click(findTestObject('sale with cheque/button_Proceed To Payment'), FailureHandling.STOP_ON_FAILURE)

