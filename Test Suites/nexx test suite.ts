<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>nexx test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>79356752-d570-426c-9604-7949794ec355</testSuiteGuid>
   <testCaseLink>
      <guid>58e8da6b-4040-4d05-a1d2-05612627dd93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/cash and cheque</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>daf484f2-de68-4a28-8a95-a4122233e739</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/cash credit and cheque</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8287542b-eb44-47fe-a7bb-747b58368892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/cash credit card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3be5701f-08fd-4042-836f-4a9d11b494ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/credit and cheque</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46574176-39b7-46fd-bb7a-aeb492a0b436</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/credit note</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c07dd1aa-52c6-43fc-9b18-315aef8cfad4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/discount and cash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b57fd3d1-1785-4870-96e5-d02bc1bafa98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SALE with cash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33f9eea3-d24e-4f88-9b8e-08bd9a170d90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/saleswith creditcard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>811ec005-fab5-4e1b-aa32-c1ba5d95915d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/cash credit and cheque</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25b358fe-0904-436a-94e8-65789220cee0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/cash credit card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>883b3208-50d0-4c6a-8e0a-246dfc22cb8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/credit and cheque</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c6ee449-c18f-443a-aaf4-f0efd314c33b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/credit note</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ca4e5cf-ece3-491c-83d3-1429af99e6f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/discount and cash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>800c1d57-1e1a-4054-8908-406b4ee89f24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/invoice customer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25c2df25-e21d-499b-938a-cde913c92a91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SALE with cash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7018ef5-39eb-4f4a-a5db-8621d2df333b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/saleswith creditcard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c7fcc50-caee-489e-baad-1c0e336a146d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/invoice customer</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
